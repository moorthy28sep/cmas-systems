import React from 'react';

export default function Input(props) {

  return (
    <div className="form-group">
        <label>{props.label}</label>
        <input type={props.type} className="form-control" id={props.id} min="1" />
    </div>
  );
}