import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ContainedButtons from '../../modules/Buttons';

import Stepper1 from '../Stepper/Stepper1';
import Stepper2 from '../Stepper/Stepper2';
import Stepper3 from '../Stepper/Stepper3';

import './Stepper.scss';

class Stepper extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      step1 : 'block',
      step2 : 'hidden',
      step3 : 'hidden'
     };
}

  stepper1Fn = () => {
    this.setState(
    {
      step1 : 'block',
      step2 : 'hidden',
      step3 : 'hidden'
    });
  }

  stepper2Fn = () => {
    this.setState(
    {
      step1 : 'hidden',
      step2 : 'block',
      step3 : 'hidden'
    });
  }

  stepper3Fn = () => {
    this.setState(
    {
      step1 : 'hidden',
      step2 : 'hidden',
      step3 : 'block'
    });
  }

  render() {
        
    return (
        <section>
            <Container>
            <Row>
                <Col xs={12}>
                  <h2>Assessment</h2>
                  <div className={this.state.step1}>
                    <h3>Step 1</h3>
                    <Stepper1 />
                    <div className={"button-container"}>
                      <ContainedButtons name={'Step 2'}  onClick={this.stepper2Fn}/>
                    </div>            
                  </div>
                  <div className={this.state.step2}>
                    <h3>Step 2</h3>
                    <Stepper2 />
                    <div className={"button-container"}>
                      <ContainedButtons name={'Step 3'} onClick={this.stepper3Fn} />
                    </div>            
                  </div>
                  <div className={this.state.step3}>
                    <h3>Step 3</h3>
                    <Stepper3 />
                    <div className={"button-container"}>
                      <ContainedButtons name={'Step 1'} onClick={this.stepper1Fn} />
                    </div>            
                  </div>       
                </Col>
            </Row>
            </Container>
      </section>
    );
  }
}

export default Stepper;