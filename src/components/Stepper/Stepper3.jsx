import React, { Component } from 'react';
import ContainedButtons from '../../modules/Buttons';
import Input from '../../modules/Input';
import Results from '../../modules/Results';

import './Stepper.scss';

class Stepper3 extends Component {

    Stepper3Fn = () => 
    {
      let a;
      a=document.getElementById("number-input3").value;
	    let b=Number(a), cmas=0, systems=0, cmassystems=0, good=0, num=0;
      var out = "";
	
      if(isNaN(a)|| a===" ")
      {
        alert(`Please enter number in the given Input`);
      }
      else
      {      
        for(let i=1;i<=b;i++)
        {
          i= i.toString();
          if(i.includes("3"))
          {
              out += `good `;
          good=good+1;
          }	
          else if(i%15===0)
          {
            out+= `CMASsystems `;
            cmassystems=cmassystems+1;
          }          
          else if(i%3===0)
          {
            out+= `CMAS `;
            cmas=cmas+1;
          }
          else if(i%5===0)
          {
            out+= `systems `;
            systems=systems+1;          
          }
          else
          {
            out+= i+" ";
            num=num+1;			  
          }		 
          }		 
        }
      document.getElementById("results-items3").innerHTML=out;
      document.getElementById("cmas").innerHTML="<br>"+"CMAS: "+cmas;
      document.getElementById("systems").innerHTML="<br>"+"systems: "+systems;
      document.getElementById("cmassystems").innerHTML="<br>"+"CMASsystems: "+cmassystems;
      document.getElementById("good").innerHTML="<br>"+"good: "+good;
      document.getElementById("num").innerHTML="<br>"+"integer: "+num;
    }

  render() {
        
    return (        
        <div className="stepper">
            <p>Enhance your existing solution to perform the following: Produce a report at the end of the program showing how many times the following were output <strong>CMAS systems CMASsystems good</strong> and <strong>integer</strong> (for the numbers).</p>
            <form>
              <Input label={'Enter Number'} type={'number'} id={'number-input3'} />
              <ContainedButtons onClick={this.Stepper3Fn} className="btn btn-primary" name={'Result'} />                    
              <div className="results">
                <Results id={'results-items3'} />
                <Results id={'cmas'} />
                <Results id={'systems'} />     
                <Results id={'cmassystems'} />
                <Results id={'good'} />
                <Results id={'num'} />
              </div>
            </form>            
        </div>
    );
  }
}

export default Stepper3;