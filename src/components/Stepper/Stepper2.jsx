import React, { Component } from 'react';
import ContainedButtons from '../../modules/Buttons';
import Input from '../../modules/Input';
import Results from '../../modules/Results';

import './Stepper.scss';

class Stepper2 extends Component {
 
    Stepper2Fn = () => 
    {
      let a;	
      a=document.getElementById("number-input2").value;    
      let b=Number(a);              
      var out = "";      
      if(isNaN(a)|| a===" ")
      {
        alert(`Please enter number in the given Input`);
      }
      else
      {      
        for(let i=1;i<=b;i++)
        {
          i= i.toString();
          if(i.includes("3"))
          {
            out += `good `;
          }		                            
          else if(i%15===0)
          {
            out+= `CMASsystems `;
          }          
            else if(i%3===0)
          {
            out+= `CMAS `;
          }
          else if(i%5===0)
          {
            out+= `systems `;
          }                              
          else
          {
            out+= i+" ";
          }         
        }		 
      }
        document.getElementById("results-items2").innerHTML=out;
    }

  render() {
        
    return (        
        <div className="stepper">
            <p>Enhance your existing CMASsystems solution to perform the following:  If the number contains a three you must output the text <strong>'good'</strong>. </p>
            
            <form>            
              <Input label={'Enter Number'} type={'number'} id={'number-input2'} />
              <ContainedButtons onClick={this.Stepper2Fn} className="btn btn-primary" name={'Result'} />                    
              <div className="results">                  
                  <Results id={'results-items2'} />
              </div>
            </form>

        </div>
    );
  }
}

export default Stepper2;