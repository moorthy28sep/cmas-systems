import React, { Component } from 'react';
import ContainedButtons from '../../modules/Buttons';
import Input from '../../modules/Input';
import Results from '../../modules/Results';


import './Stepper.scss';

class Stepper1 extends Component {
 
    Stepper1Fn = () => 
    {
        let a;        
        a=document.getElementById("number-input").value;    
        a=Number(a);            
        var out = "";        
        if(isNaN(a)|| a===" ")
        {
            alert(`Please enter number in the given Input`);
        }
        else
        {        
            for(let i=1;i<=a;i++)
            {
              if(i%15===0)
              {
                out += `CMASsystems `; 
              }		    
              else if(i%5===0)
              {
                out += `systems `;
              }
              else if(i%3===0)
              {
                out += `CMAS `;
              }
              else
              {
                out+= i+" ";
              }             
            }		 
        }
           document.getElementById("results-items").innerHTML=out;
    }

  render() {
        
    return (        
        <div className="stepper">
            <p>Write some code that prints out the following for a contiguous range of numbers:  the name <strong>'CMAS'</strong> for numbers that are multiples of 3, <strong>'systems'</strong> for numbers that  are multiples of 5, <strong>'CMASsystems'</strong> for numbers that are multiples of 15</p>
            
            <form>
                <Input label={'Enter Number'} type={'number'} id={'number-input'} />            
                <ContainedButtons onClick={this.Stepper1Fn} className="btn btn-primary" name={'Result'} />                    
                <div className="results">
                    <Results id={'results-items'} />                 
                </div>
            </form>
                        
        </div>
    );
  }
}

export default Stepper1;