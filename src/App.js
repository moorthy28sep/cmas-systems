import React from 'react';
import './App.scss';
import HeaderBar from './modules/HeaderBar';
import Stepper from './components/Stepper/Stepper';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HeaderBar />
      </header>
      <Stepper />
    </div>
  );
}

export default App;
